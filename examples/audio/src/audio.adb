with Ada.Numerics;     use Ada.Numerics;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO;      use Ada.Text_IO;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;

with Ada.Exceptions;   use Ada.Exceptions;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C; use Interfaces.C;

with GNAT.Source_Info; use GNAT.Source_Info;

with gsl.vector_double ;
with gsl.version ;
with gsl.fft.real ;
with gsl.fft.halfcomplex ;
with gsl.complex ;
with gsl.complex.math ;

with sndfile;

procedure Audio is
   use gsl ;
   use gsl.double_text_io;

   verbose : boolean := true;
   myname : constant String := gnat.Source_Info.enclosing_entity ;
   Status : Int ;

begin
   if Verbose
   then
      Put(myname); New_Line ;
      Put("sndfile version : "); Put_Line(Interfaces.C.Strings.Value(sndfile.Version));
      Put("gsl version : "); Put_Line(Value(gsl.version.library));
   end if ;

   if ARGUMENT_COUNT = 0
   then
      Put_Line("usage: audio audiofile [start_time [timespan]]");
      return ;
   end if ;
   declare
      soundfilename : constant String := Argument(1) ;
      soundfile : sndfile.File_Type ;
      info : sndfile.SF_INFO;

      fragment : sndfile.AudioFragment_Type;
      t : float := 0.0 ;
      tspan : float := 1.0/64.0 ; 
   begin
      soundfile := sndfile.Open(soundfilename);
      info := sndfile.Info(soundfile);

      if ARGUMENT_COUNT > 1
      then
         t := float'value( ARGUMENT(2) );
         if ARGUMENT_COUNT > 2
         then
            tspan := float'value( ARGUMENT(3) );
         end if ;
      end if ;
      Put_Line("Audio file opened");
      fragment := sndfile.Read(soundfile,t,tspan);
      declare
         ch1 : access gsl.vector_double.gsl_vector
                     := gsl.vector_double.To_C(fragment.ch1.all) ;
         ch2 : access gsl.vector_double.gsl_vector ;
      begin
         if info.channels > 1
         then
            ch2 := gsl.vector_double.To_C(fragment.ch2.all);
         end if ;
         gsl.vector_double.WriteCSV(ch1,soundfilename & ".csv",ch2);
         declare
            N : Integer := Integer(ch1.size) ;
            ws : access gsl.fft.real.gsl_fft_real_workspace := gsl.fft.real.workspace_alloc( size_t(N) );
            wt : access gsl.fft.real.gsl_fft_real_wavetable := gsl.fft.real.wavetable_alloc( size_t(N) );
            wth : access gsl.fft.halfcomplex.gsl_fft_halfcomplex_wavetable := gsl.fft.halfcomplex.wavetable_alloc( size_t(N) );
            freqs : gsl.complex.gsl_complex_complex_array(0..N-1) 
                     := (others => (0.0,0.0)) ;
            logfile : File_Type ;
         begin
            Status := gsl.fft.real.transform(ch1,wt,ws);
            Status := gsl.fft.halfcomplex.unpack(ch1,freqs);
            Create(logfile,Out_File,myname & ".freq.csv");
            Set_Output(logfile);
            for i in 1..N/2
            loop
               Put(i) ; Sep ;
               Put(fragment.ch1(i)); Sep ;
               gsl.complex.Print(freqs(i));
               Put(gsl.complex.math.cabs(freqs(i))*2.0/double(N)) ;
               New_Line;
            end loop ;
            Set_Output(Standard_Output);
            Close(logfile);            
         end ;
      end ;

   end ;
   exception
      when e : others => 
         Put("Exception reading: "); 
         Put(Exception_Message(e)); 
         New_Line;
end Audio;
