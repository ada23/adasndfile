set term png size 1000,1000
set datafile separator ";"
set output output

set title "Spectrum of the Audio Fragment"
plot filename using 1:7 title "Freq multiplier"  with impulse lw 16